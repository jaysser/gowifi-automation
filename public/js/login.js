$(function(){
   document.title = "Login | GOWifi Test Automation";
   
   if(sessionStorage.getItem("alert") !== null) {
      var message = sessionStorage.getItem("alert");
      message = JSON.parse(message);
      if(message.type == "signup")
         toastr.success(message.message, "Registration Successful");
      if(message.type == "forgot_pass")
         toastr.success(message.message);
      
      sessionStorage.clear();
   }
   
   $('.form_btn').on('click', function(){
      var _this = $(this).data('value');
      $('.card').find('form').addClass('d-none');
      $('form[name='+_this+']').removeClass('d-none');
      switch (_this) {
         case "login": $('h1.title').text('Login'); break;
         case "signup": $('h1.title').text('Register'); break;
         case "forgot_pass": $('h1.title').text('Forgot Password?'); break;
      }
   });
   
   $('form[name=login]').submit(function(e){
      e.preventDefault();
      $.ajax({
         url: base_url+"api/user/login",
         data: $(this).serialize(),
         method: 'post',
         type: 'json',
         success: function(response){
            location.reload();
         },
         error: function(response){
            toastr.error(response.responseJSON.message,"Login Failed")
         }
      });
   });
   
   $('form[name=signup]').submit(function(e){
      e.preventDefault();
      $.ajax({
         url: base_url+"api/user/create",
         data: $(this).serialize(),
         method: 'post',
         type: 'json',
         success: function(response){
            data = {
               type: "signup",
               message: response.message
            }
            sessionStorage.setItem('alert', JSON.stringify(data));
            location.reload();
         },
         error: function(response){
            toastr.error(response.responseJSON.message,"Registration Failed");
            console.log(response)
         }
      });
   });
   
   $('form[name=forgot_pass]').submit(function(e){
      e.preventDefault();
      $.ajax({
         url: base_url+"api/password/forgot",
         data: $(this).serialize(),
         method: 'post',
         type: 'json',
         success: function(response){
            data = {
               type: "forgot_pass",
               message: response.message
            }
            sessionStorage.setItem('alert', JSON.stringify(data));
            location.reload();
         },
         error: function(response){
            toastr.error(response.responseJSON.message,"Password Retrieval Failed");
            console.log(response)
         }
      });
   });
});