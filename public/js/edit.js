$(function(){
	var file = [];
	var i = 0;
   document.title = "Edit API Collections | GOWifi Test Automation";
   $(".navbar-nav .nav-link.active").removeClass("active");
   $(".navbar-nav .nav-link#view").addClass("active");
   
   var url = window.location.href.replace(/\/\s*$/,'').split('/'); 
   url.shift(); 
   collection_name = url[url.length-1];
  
   $.getJSON(base_url+'api/test/' + collection_name,function(data){
      var content = JSON.stringify(data,null,2);
      $('input[name=collection]').val(collection_name);
      $('textarea[name=content]').val(content);
   });
   
   $('form[name=edit-collection]').submit(function(e){
      e.preventDefault();
      $('#update-confirm').modal('hide')
      
      var data = {
         filename: collection_name,
         data: JSON.stringify(JSON.parse($('textarea[name=content]').val()))
      } 
      
      if($('input[name=collection]').val() !== collection_name){
         new_name = {newname: $('input[name=collection]').val()};
         $.extend(data,new_name);
      }
      
      $.ajax({
         url: base_url+'api/file/update',
         data: JSON.stringify(data),
         type: "post",
         contentType: 'application/json; charset=utf-8',
         dataType: 'json',
         success: function(data){
            toastr.success(data.message);
         },
         error: function(response) {
            console.log(response)
            lastToastr = (response.responseJSON) ? toastr['error'](response.responseJSON.message) : toastr['error'](response.statusText, "Error: "+response.status);
         }
      })
   });
   
});