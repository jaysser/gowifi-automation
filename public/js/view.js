$(function(){
	var file = [];
	var i = 0;
   document.title = "View API Collections | GOWifi Test Automation";
   $(".navbar-nav .nav-link.active").removeClass("active");
   $(".navbar-nav .nav-link#view").addClass("active");
   
   var dtTable = $('table#collections').DataTable({
      "ajax": {
      			"url" : base_url+'api/get/collections',
      			"type" : "GET",
      },
      "columns": [
            { "data": "title" },
            { "data": "no_api"},
            { "data": "date_created" },
            { "data" : "filename",render: function(data){
            	return `<div class="dropdown">
                 <a class="" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <i class="material-icons">more_vert</i>
                 </a>

                 <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                   <a class="dropdown-item" href="`+base_url+`run/collection?collection=`+data+`">Run</a>
                   <a class="dropdown-item" href="`+base_url+`edit/collection/`+data+`">Edit</a>
                   <a class="dropdown-item deleteCollection" data-filename=`+data+` href="#" data-toggle="modal" data-target="#deleteCollection">Delete</a>
                 </div>
               </div>`;
            }}
        ],
      "ordering": false,
      "initComplete" : function(settings,json){
        $('.deleteCollection').on('click',function(){
          var filename = $(this).attr('data-filename');

          $(this).closest('tr').addClass('selected');
          $('#deleteCollection .btn-danger').attr('data-filename',filename);
          $('#deleteCollection .btn-danger').attr('data-api',false);
          $('#deleteCollection .modal-title').text('Deleting Collection');
          $('select[name=collections_length]').addClass('custom-select');
        });
      }
   });
   
   var dtTable = $('#apis').DataTable({
      "ajax": {
      			"url" : base_url+'api/get/collections/1',
      			"type" : "GET",
      },
      "columns": [
            { "data": "title" },
            { "data": "path"},
            { "data": "method"},
            { "data": "date_created" },
            { "data" : "filename",render: function(data){
            	return `<div class="dropdown">
                 <a class="" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <i class="material-icons">more_vert</i>
                 </a>

                 <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                   <a class="dropdown-item" href="`+base_url+`run/api?api=`+data+`">Run</a>
                   <a class="dropdown-item" href="`+base_url+`edit/api/`+data+`">Edit</a>
                   <a class="dropdown-item deleteApi" data-api=true data-filename=`+data+` href="#" data-toggle="modal" data-target="#deleteCollection">Delete</a>
                 </div>
               </div>`;
            }}
        ],
      "ordering": false,
      "initComplete" : function(settings,json){
        $('.deleteApi').on('click',function(){
          var filename = $(this).attr('data-filename');

          $(this).closest('tr').addClass('selected');
          $('#deleteCollection .btn-danger').attr('data-filename',filename);
          $('#deleteCollection .btn-danger').attr('data-api',true);
          $('#deleteCollection .modal-title').text('Deleting API');
          $('select[name=collections_length]').addClass('custom-select');
        });
      }
   });
      
   $('#deleteCollection .btn-danger').on('click',function(){
     var filename = $(this).attr('data-filename');
     var api      = $(this).attr('data-api');
     var params   = {
                      "filename" : filename,
                      "api" : api, 
                    };

     $.ajax({
         url: base_url+"api/file/delete",
         data: JSON.stringify(params),
         type: "post",
         contentType: 'application/json; charset=utf-8',
         dataType: 'json',
         complete: function(data) {
           if(api == "true"){
             lastToastr = toastr['success']('Api has been deleted.');
             $('#deleteCollection').modal('hide');
             $('#apis tr.selected').remove();
           }else{
             lastToastr = toastr['success']('Collection has been deleted.');
             $('#deleteCollection').modal('hide');
             $('#collections tr.selected').remove();
           } 
         }
     })
   });
   
   if (sessionStorage.getItem("adding_collection_msg") !== null) {
      var addMsg = sessionStorage.getItem("adding_collection_msg");
      toastr.success(addMsg);
      sessionStorage.removeItem("adding_collection_msg");
   }
   
   if(getParameterByName("t") != null && getParameterByName("t") == 1){
      $('.nav-item .nav-link.active,.tab-content .tab-pane.active').removeClass("active");
      $('.nav-item .nav-link#api-tab,.tab-content .tab-pane#api').addClass("active");
   }
});


