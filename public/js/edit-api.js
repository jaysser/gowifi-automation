$(function(){
	var file = [];
	var i = 0;
   document.title = "Edit API Collections | GOWifi Test Automation";
   $(".navbar-nav .nav-link.active").removeClass("active");
   $(".navbar-nav .nav-link#view").addClass("active");
   
   var url = window.location.href.replace(/\/\s*$/,'').split('/'); 
   url.shift(); 
   apiName = url[url.length-1];
   
   
   $.getJSON(base_url+'api/test/' + apiName + '/1',function(data){
      var content = JSON.stringify(data,null,2);
      $('input[name=api_name]').val(data[0].api);
      $('input[name=endpoint]').val(data[0].path);
      $('input[name=method]').filter("[value="+data[0].method+"]").attr('checked',true);
      $('textarea[name=params]').val(function(fData){
         var val="";
         $.each(data[0].params,function(key,value) {
            val += key + " : " + value + "\n"
         })
         return val;
      });
   });
   
   $('form[name=edit-api]').submit(function(e){
      $('.modal').modal('hide');
      
      if(validateUrl($("input[name=endpoint]").val()) == false) {
         toastr.error("API Url must be a complete valid url (eg. http://api.com/api)")
         return false;
      }
      
      
      e.preventDefault();
      $('#update-confirm').modal('hide')
      var content = $('textarea[name=params]').val().trim();
      
      
      var data = {
         api: apiName,
         path: $("input[name=endpoint]").val(),
         method: $("input[name=method]:checked").val(),
         params: parseKeyPairtoJSON(content)
      } 
      
      if($('input[name=api_name]').val() !== apiName){
         new_name = {api_new: $('input[name=api_name]').val()};
         $.extend(data,new_name);
      }
      
      $.ajax({
         url: base_url+'api/file/update/api',
         data: JSON.stringify(data),
         type: "post",
         contentType: 'application/json; charset=utf-8',
         dataType: 'json',
         success: function(data){
            toastr.success(data.message);
         },
         error: function(response) {
            console.log(response)
            lastToastr = (response.responseJSON) ? toastr['error'](response.responseJSON.message) : toastr['error'](response.statusText, "Error: "+response.status);
         }
      })
   });
   
});