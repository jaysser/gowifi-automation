$(function(){
	document.title = "Add API Collections | GOWifi Test Automation";
   var placeholder = "Key_1: Value 1\nKey_2: Value 2";
   $("textarea").attr("placeholder", placeholder)
   $(".navbar-nav .nav-link.active").removeClass("active");
   $(".navbar-nav .nav-link#view").addClass("active");
   
   $('form[name=add-collection]').submit(function(e){
      e.preventDefault();
      var api_name = $('input[name=api_name]').val();
      var endpoint = $('input[name=endpoint]').val();
      var method = $('input[name=method]').val();
      var body = $('textarea[name=params]').val();
      body = getQueryParams(body.replace(/\n/g,"&").replace(/:/g,"="));
      data = {
         api_name: api_name,
         endpoint: endpoint,
         method: method,
         params: body
      }
      
      $.ajax({
         url: base_url+"api/file/create/api",
         data: JSON.stringify(data),
         type: "post",
         contentType: 'application/json; charset=utf-8',
         dataType: 'json',
         success: function(data){
            if(typeof(Storage) !== "undefined") {
               sessionStorage.setItem("adding_collection_msg",data.message);
               window.location.replace(base_url+'view?t=1');
            }
            else {
               toastr.success(data.message,'',{onHidden:function(){window.location.replace(base_url+'view')}});
            }
         },
         error: function(response) {
            console.log(response)
            lastToastr = (response.responseJSON) ? toastr['error'](response.responseJSON.message) : toastr['error'](response.statusText, "Error: "+response.status);
         }
      });
   });   
   
   function getQueryParams (query) {
     return query.replace(/^\?/, '').split('&').reduce((json, item) => {
       if (item) {
         item = item.split('=').map((value) => decodeURIComponent(value))
         json[item[0]] = item[1]
       }
       return json
     }, {})
   }
});