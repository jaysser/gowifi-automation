$(function(){
   // toggle show password
   $('.toggle_pass').on('mousedown', function(){
      var _this = $(this);
      _this.html('<i class="fa fa-eye" aria-hidden="true"></i>');
      _this.parent().prev('.pass_input').attr('type','text')
   });
   $('.toggle_pass').on('mouseup mouseleave', function(){
      var _this = $(this);
      _this.html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
      _this.parent().prev('.pass_input').attr('type','password')
   });
   
   $('form[name=change_password]').on('submit', function(e){
      e.preventDefault();
      var _this = $(this),error = [];
      $('#error_list').remove();
      
      // check if has error
      $('.pass_input').each(function(key, _this_input){
         var id = $(_this_input).attr('id');
         switch(id) {
            case "new_pass" : 
               if($(_this_input).val() === $('input[name=password]').val())
                  error.push('New password must not be the same as your current password.');
            break;
            
            case "confirm_pass": 
               if($(_this_input).val() !== $('input[name=new_password]').val())
                  error.push('Password mismatched.');
            break;
            
         }
      });
      
      // display error message/s
      if(error.length > 0) {
         var i;
         error_output = '<ul class="fa-ul list" id="error_list">';
         for(i=0; i<error.length; i++) {
            error_output += '<li class="text-warning small"><i class="fa-li fa fa-exclamation-triangle"></i> '+error[i]+'</li>';
         }
         error_output += '</ul';
         $('#errors').append(error_output);
         error = [];
      }
      
      // submit if all is valid
      if(_this.find('#error_list').length == 0) {
         $.ajax({
            url: base_url+'api/password',
            method: 'post',
            data: _this.serialize(),
            type: 'json',
            success: function(response){
               console.log(response);
               $('#change_pass.modal').modal('hide');
               toastr.success(response.message);
            },
            error: function(response){
               console.log(response);
               _this.find('#errors').append('<ul class="fa-ul list" id="error_list"><li class="text-warning small"><i class="fa-li fa fa-ban"></i> '+response.responseJSON.message+'</li></ul>');
            }
         })  
      }
   });
   
   $('#change_pass.modal').on('hidden.bs.modal', function (e) {
      $(this).find('form[name=change_password]').trigger('reset');
      $('#error_list').remove();
   })
});