$(function(){
	document.title = "Add API Collections | GOWifi Test Automation";
   $(".navbar-nav .nav-link.active").removeClass("active");
   $(".navbar-nav .nav-link#view").addClass("active");
   
   $('form[name=add-collection]').submit(function(e){
      e.preventDefault();
      var filename = $('input[name=filename]').val();
      var body = JSON.parse($('textarea[name=body]').val());
      console.log(body);
      body = JSON.stringify(body);
      data = {
         filename: filename,
         data: body
      }
      $.ajax({
         url: base_url+"api/file/create",
         data: JSON.stringify(data),
         type: "post",
         contentType: 'application/json; charset=utf-8',
         dataType: 'json',
         success: function(data){
            if(typeof(Storage) !== "undefined") {
               sessionStorage.setItem("adding_collection_msg",data.message);
               window.location.replace(base_url+'view');
            }
            else {
               toastr.success(data.message,'',{onHidden:function(){window.location.replace(base_url+'view')}});
            }
         },
         error: function(response) {
            console.log(response)
            lastToastr = (response.responseJSON) ? toastr['error'](response.responseJSON.message) : toastr['error'](response.statusText, "Error: "+response.status);
         }
      });
   });   
});