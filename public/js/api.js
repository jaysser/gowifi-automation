$(function(){
   document.title = "GOWifi Test Automation";
   var collection = getParameterByName('collection') ? getParameterByName('collection'):false;

   $(".navbar .nav-link.active").removeClass("active");
   $(".navbar .nav-link#run").addClass("active");
   
   $.getJSON(base_url+'api/get/collections',function(data){
      var i,option;
      var selected = false;
      for(i=0;i<data.data.length;i++) {
         option = option+"<option "+(collection == data.data[i].filename ? "selected":"")+" value='"+data.data[i].filename+"'>"+data.data[i].title+"</option>";
         if(collection == data.data[i].filename){
            getConfigFile(data.data[i].filename);
            $("textarea[name=params]").attr("rows",20)
         }
      }
      if(collection != false)
         $('select').html('<option value=""  disabled>Choose a collection</option>'+option)
      else
         $('select').html('<option value=""  selected disabled>Choose a collection</option>'+option)
   });
      
   $('select').on('change', function(){
      getConfigFile($(this).val());
      $("textarea[name=params]").attr("rows",20)
   });
   
   $('form[name=collection_test]').on('submit',function(e){
      e.preventDefault();
      $('body').css('overflow','hidden');
      $('#alerts').html('');
      $('body').append('<div class="loader"></div>');
            
      $('#test-form').addClass('mx-auto');
      $('#test-result').addClass('d-none');
      
      collection = $('select').val();
      input = $('textarea[name=params]').val();
      email_address = $('input[name=email_address]').val();
      env = $('input[name=env]').val();
      params = {
                  'params':JSON.stringify(getQueryParams(input.replace(/\n/g,"&").replace(/: /g,"="))),
                  'email_address' : email_address,
                  'env' : env,
               };
      
      $.ajax({
         url: base_url+"api/run-collection/web/"+collection,
         data: JSON.stringify(params),
         type: "post",
         contentType: 'application/json; charset=utf-8',
         dataType: 'json',
         complete: function() {
            $('body').find('.loader').remove();
            $('body').css('overflow','auto');
            $('#test-form').removeClass('mx-auto');
            $('#test-result').removeClass('d-none');
            // window.scrollTo(0,0);
            var body = $("html, body");
            body.stop().animate({scrollTop:0}, 500, 'swing', function() {});
         },
         success: function(response) {
            var i, collection_name = Object.keys(response.data)[0],
            collection = response.data[collection_name];
            
            for(i=0;i<collection.success.length;i++) {
               payload = collection.success[i];
               key = Object.keys(collection.success[i]);
               if(payload[key].code == 200 && payload[key].code < 299)
                  stat = 'success';
               else if(typeof payload[key].code == 'undefined') {
                  payload[key].code = '';
                  payload[key].message = '';
                  if(typeof payload[key].data != 'undefined' && payload[key].data != '') {
                     stat = 'success';
                  }
                  else {
                     stat = 'danger';
                  }
               }
               else
                  stat = 'danger';
                  
               var response = '';

               if(payload[key].response)
                  response = (typeof payload[key].response != "undefined") ? payload[key].response : '';

               if(payload[key].data)
                  response = payload[key].data;

               var payload_msg = '';

               if(payload[key].payload && payload[key].payload != "")
                  payload_msg = JSON.stringify(payload_msg,null,2);
               else
                  payload_msg = '[check URL]';
               
               var alert = `
               <div class="row">
                  <div class="col-12">
                     <div class="alert alert-`+ stat +` mb-2" role="alert">`+ key +`
                        <span class="float-right">
                           <a data-toggle="collapse" href="#`+ key +`" role="button" aria-expanded="false" aria-controls="`+ key +`" class="alert-link">View Details</a>
                        </span>
                     </div>
                  </div>
                  <div class="col-12">
                     <div class="collapse multi-collapse" id="`+ key +`">
                        <div class="card card-body">
                           <p><b>Status</b>: `+ payload[key].code +`</p>
                           <p><b>Message</b>:</p>
                           <pre id="message">`+ payload[key].message +`</pre>
                           <p><b>Payload</b>:</p>
                           <pre id="payload">`+ payload_msg +`</pre>
                           <p><b>Response</b>:</p>
                           <pre id="payload">`+ JSON.stringify(payload[key].data,null,2) +`</pre>
                        </div>
                     </div>
                  </div>
               </div>               
               `;
               $('#alerts').append(alert);  
            }
         },
         error: function(response) {
            var http_error = response.status;
            var response = response.responseJSON;
            var i, alert = "";
            var collection_name = Object.keys(response.data)[0];
            var collection_results = response.data[collection_name];
            var collection_failed = response.data[collection_name]['failed'];
            
            for(var key in collection_results) {
               if(key == "success") {
                  for(i=0; i<collection_results['success'].length; i++) {
                     api_name = Object.keys(collection_results.success[i])[0];
                     
                     http_code = collection_results.success[i][api_name].code;
                     status_color = (http_code == 200) ? "success" : "danger";
                     response = JSON.stringify(collection_results.success[i][api_name].data,null,2);
                     message = collection_results.success[i][api_name].message;
                     payload = JSON.stringify(collection_results.success[i][api_name].payload,null,2);
                     
                     alert += generateAlert(api_name,message,status_color,http_code,payload,response);
                     
                  }
               }
               
               if(key == "failed") {
                  api_name = collection_results.failed.api;
                  response = collection_results.failed.return.split("response:");
                  message = collection_results.failed.message;
                  payload = JSON.stringify(collection_results.failed.payload,null,2);
                  
                  alert += generateAlert(api_name,message,"danger",http_error,payload,response[0]);
               }
               
            }
           
            $('#alerts').append(alert);
         }
      });
   });
   
   function getConfigFile(file) {
      $.getJSON(base_url+'api/test/' + file,function(data){
         var param = "";
         $.each(data[0].params, function(key, value){
            param += key+": "+value+"\n";
         });
         
         //$('textarea[name=params]').val(param.trim() + "\nmobile_no: 9089814880\nrequest_fi_msisdn: 9051234567\nproduct_id: 1117\npromo_code: AUTHGRSR");
      });
   }
   
   function getQueryParams (query) {
     return query.replace(/^\?/, '').split('&').reduce((json, item) => {
       if (item) {
         item = item.split('=').map((value) => decodeURIComponent(value))
         json[item[0]] = item[1]
       }
       return json
     }, {})
   }
   
   function generateAlert(api_name,message,status_color,http_code,payload,response) {
      var alert = `
         <div class="row">
            <div class="col-12">
               <div class="alert alert-`+ status_color +` mb-2" role="alert">`+ api_name +`
                  <span class="float-right">
                     <a data-toggle="collapse" href="#`+ api_name +`" role="button" aria-expanded="false" aria-controls="`+ api_name +`" class="alert-link">View Details</a>
                  </span>
               </div>
            </div>
            <div class="col-12">
               <div class="collapse multi-collapse" id="`+ api_name +`">
                  <div class="card card-body">
                     <p><b>Status</b>: `+ http_code +`</p>
                     <p><b>Message</b>:</p>
                     <pre id="message">`+ message +`</pre>
                     <p><b>Payload</b>:</p>
                     <pre id="payload">`+ payload +`</pre>
                     <p><b>Response</b>:</p>
                     <pre id="payload">`+ response +`</pre>
                  </div>
               </div>
            </div>
         </div>               
         `
      return alert;
   }
});