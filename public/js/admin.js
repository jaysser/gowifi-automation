$(function(){
   $('#warning').on('show.bs.modal', function (e) {
      var button = $(e.relatedTarget);
      $(this).find('#body').text(button.data('message'));
      $(this).find('#action-submit').attr('data-value',button.data('value'));
   });
   
   $('#warning').on('hide.bs.modal', function (e) {
      $(this).find('#body').text('');
      $(this).find('#action-submit').attr('data-value',null);
   });
   
   $('form[name=create_user]').on('submit',function(e){
      $('body').append('<div class="loader"></div>');
      e.preventDefault();
      
      data = {
         username : $(this).find('input[name=username]').val(),
         email : $(this).find('input[name=email]').val()
      }
      
      $.ajax({
         url: base_url+"api/user/create",
         data: data,
         method: 'post',
         type: 'json',
         complete: function(data) {
            $('body').find('.loader').remove();
         },
         success: function(data){
            $('#create_user_modal').modal('hide');
            toastr.success(data.message);
            setTimeout(function(){location.reload();},1000);
         },
         error: function(response){
            toastr.error(response.responseJSON.message,"Registration Failed");
            console.log(response)
         }
      });
   });
   
   var dtTable = $('table#admin').DataTable({
      "ajax": {
      			"url" : base_url+'api/user/list',
      			"type" : "GET",
      },
      "columns": [
   			{ "data": "username"},
            { "data": "username" },
            { "data": "email"},
            { "data": "collection" },
            { "data": "api"}
        ],
      'columnDefs': [
	      {
	         'targets': 0,
	         'data' : 'username',
	         'checkboxes': {
	            'selectRow': true
	         }
	      }
	   ],
	  'select': {
         'style': 'multi'
      },
      'order': [[1, 'asc']],
      "ordering": false,
      "initComplete" : function(settings,json){
         $('input[type=checkbox]').on('change', function(){
            var rows_selected = dtTable.column(0).checkboxes.selected(), username = [];
            $.each(rows_selected, function(index, rowId){
               username.push(rowId);
            });
            if(username.length > 0) {
               $('button[data-target="#warning"]').removeAttr('disabled');
            }
            else {
               $('button[data-target="#warning"]').attr('disabled',true);
            }
         });
         $('#action-submit').on('click',function(e){
            e.preventDefault();
            $('body').append('<div class="loader"></div>');
            var action = $(e.target).attr('data-value');
            var rows_selected = dtTable.column(0).checkboxes.selected();
            var username = [];
            var action_url;
            
            // Iterate over all selected checkboxes
            $.each(rows_selected, function(index, rowId){
               username.push(rowId);
            });
            
            if(action == 0)
               action_url = base_url+"api/password/reset"; 
            else
               action_url = base_url+"api/user/delete"; 

            $.ajax({
               url: action_url,
               data: JSON.stringify({"username" : username}),
               type: "post",
               contentType: 'application/json; charset=utf-8',
               dataType: 'json',
               complete: function(data) {
                  $('#warning').modal('hide');
                  $('body').find('.loader').remove();
               },
               success: function(data) {
                  toastr.success(data.message);
                  setTimeout(function(){location.reload();},500);
               },
               error: function(data) {
                  if(data.responseJSON.length != 0)
                     toastr.error(data.message[0]);
                  else
                     toastr.error("Something went wrong. Please try again.");
               }
            });
         });
      }
   });
});


