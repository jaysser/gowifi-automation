@include('header')
<section>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-8 mx-auto">
         <div class="card">
         <div class="card-body">
            <form name="edit-collection">
               <div class="form-group">
                  <label for="exampleInputEmail1">Collection Name (collection_name)</label>
                  <input type="text" class="form-control" name="collection" placeholder="Enter Collection Name Here">
               </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Body</label>
                  <textarea class="form-control" name="content" rows="25" spellcheck="false" placeholder="Add API Collections here" style="font-size:13px;font-family:'Courier New', san-serif" required></textarea>
               </div>
               <a href="{{ env('BASE_URL') }}view" class="btn btn-secondary">Back to list</a>
               <button type="button" class="btn btn-primary btn-raised" data-toggle="modal" data-target="#update-confirm">Save</button>
               
               <div class="modal fade" id="update-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog" role="document">
                   <div class="modal-content">
                     <div class="modal-header">
                       <h5 class="modal-title" id="exampleModalLabel">Updating Collection</h5>
                     </div>
                     <div class="modal-body">
                       Are you sure you want to continue?
                     </div>
                     <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                       <button type="submit" class="btn btn-primary btn-raised">Save changes</button>
                     </div>
                   </div>
                 </div>
               </div>
            </form>
         </div>
         </div>
         </div>
      </div>
   </div>
</section>
<script type="text/javascript" src="{{env('BASE_URL')}}public/js/edit.js"></script>
@include('footer')