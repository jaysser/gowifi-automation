@include('header')
<section>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-11 mx-auto">
            <div class="row">
               <div class="col-md-6 mx-auto" id="test-form">
               <div class="card">
               <div class="card-body">
                  <form name="api_test">
                     <div class="form-group">
                        <label for="exampleInputEmail1">API</label>
                        <select class="custom-select" name="api" required>
                        </select>
                     </div>
                     <div class="form-group">
                        <label for="exampleInputEmail1">API URL</label>
                        <input name="path" class="form-control-plaintext" style="font-weight:normal" placeholder="http://" readonly required />
                     </div>
                     <div class="form-group">
                        <label for="exampleInputEmail1">Send Result To: <small>(<i>qa1@yondu.com,qa2@yondu.com,qa3@yondu.com</i>)</small></label>
                        <input class="form-control" name="email_address" placeholder="dev@mygowifi.com" value="rvasquez@yondu.com" required>
                     </div>
                     <label for="method">Method</label>
                     <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" name="method" id="post" class="custom-control-input" value="POST" checked required>
                           <label class="custom-control-label" for="post">POST</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" name="method" id="get" class="custom-control-input" value="GET" required>
                           <label class="custom-control-label" for="get">GET</label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="exampleInputEmail1">Parameters</label>
                        <textarea class="form-control" name="params" rows="15" spellcheck="false" placeholder="Choose an api from options above" style="font-size:13px;font-family:'Courier New', san-serif"></textarea>
                     </div>
                     <button type="submit" class="btn btn-primary btn-raised">Submit</button>
                  </form>
               </div>
               </div>
               </div>
               <div class="col-md-6 d-none"  id="test-result">
                  <h5>Test Results</h5>
                  <div id="alerts"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script type="text/javascript" src="{{env('BASE_URL')}}public/js/run-api.js"></script>
@include('footer')
</body>
</html>