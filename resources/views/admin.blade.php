@include('header')
<section>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-11 mx-auto">
        <!--  <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
               <a class="nav-link active" id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="users" aria-selected="false">Users</a>
            </li>
         </ul> -->
         <div class="tab-content" id="myTabContent">
            <div class="tab-pane active" id="users" role="tabpanel" aria-labelledby="users-tab">
               <div class="card">
                  <div class="card-body">                  
                     <div class="row">
                        <div class="col-md-6 my-3 ">
                           <button type="button" class="btn btn-primary btn-raised" data-value=0 data-message="Are you sure you want to continue?" data-toggle="modal" data-target="#warning" name="reset" id="reset_password" disabled>Reset Password</button>
                           <button type="button" class="btn btn-danger btn-raised" data-value=1 data-message="Are you sure you want to delete" data-toggle="modal" data-target="#warning" name="delete" id="delete_user" disabled>Delete User</button>
                        </div>
                        <div class="col-md-6 my-3 text-right">
                           <button type="button" class="btn btn-primary btn-raised" data-value=1 data-message="Are you sure you want to delete" data-toggle="modal" data-target="#create_user_modal" name="create" id="create_user">Add User</button>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <table id="admin" class="table">
                              <thead>
                                 <tr>
                                    <th width="5%"></th>
                                    <th width="40%">Username</th>
                                    <th width="20%">Email Address</th>
                                    <th width="20%">No. of Collections</th>
                                    <th width="20%">No. of APIs</th>
                                 </tr>
                              </thead>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
      </div>
   </div>
</section>
<div class="modal fade" id="warning" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="title">Warning</h5>
         </div>
         <div class="modal-body" id="body"></div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary btn-raised" id="action-submit">Confirm</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="create_user_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <form name="create_user">
            <div class="modal-header">
               <h5 class="modal-title" id="title">Create a user</h5>
            </div>
            <div class="modal-body pb-2" id="body">
               <div class="form-group mb-3">
                  <label for="collection_name">Username</label>
                  <input type="text" class="form-control classic" name="username" required>
               </div>
               <div class="form-group mb-4">
                  <label for="collection_name">Email</label>
                  <input type="email" class="form-control classic" name="email" required>
               </div>
               <div class="form-group mb-0 text-right">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-primary btn-raised" name="create_submit">Add</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{env('BASE_URL')}}public/js/admin.js"></script>
<script type="text/javascript">
   var base_url = "{{env('BASE_URL')}}";
    (function(window, undefined){
        window.jQuery = window.$ = jQuery;
    })(window);
</script>
@include('footer')