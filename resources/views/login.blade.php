@include('header')
<style>
@import url('https://fonts.googleapis.com/css?family=Raleway:300');
body{
background: url('resources/assets/bg-main.jpg') center center no-repeat;
background-size: cover;
height: 100vh
}
h1.title {
font-family: 'Raleway', sans-serif;
color: #fff;
text-align: center
}
</style>

<section>
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-4 col-md-8 mx-auto mt-5">
            <h1 class="title mb-3">Login</h1>
            <div class="card">
               <div class="card-body p-3">
                  <form name="login">
                     <div class="input-group mb-3">
                        <div class="input-group-prepend">
                           <span class="input-group-text" id="basic-addon1"><i class="material-icons">account_circle</i></span>
                        </div>
                        <input type="text" class="form-control" name="username" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1"  required>
                     </div>
                     <div class="input-group mb-4">
                        <div class="input-group-prepend">
                           <span class="input-group-text" id="basic-addon2"><i class="material-icons">lock</i></span>
                        </div>
                        <input type="password" class="form-control" name="password" placeholder="Password" aria-label="password" aria-describedby="basic-addon2"  required>
                     </div>
                     
                     <button type="submit" class="btn btn-primary btn-raised w-100">Login</button>
                     <div class="row">
                        <div class="col-md-6"><button type="button" class="btn btn-primary w-100 form_btn" data-value="forgot_pass">Forgot Password</button></div>
                        <div class="col-md-6"><button type="button" class="btn btn-primary w-100 form_btn" data-value="signup">Sign Up</button></div>
                     </div>
                  </form>
                  <form name="forgot_pass" class="d-none">
                     <div class="input-group mb-3">
                        <div class="input-group-prepend">
                           <span class="input-group-text" id="basic-addon1"><i class="material-icons">alternate_email</i></span>
                        </div>
                        <input type="email" class="form-control" name="email" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1"  required>
                     </div>                     
                     <button type="submit" class="btn btn-primary btn-raised w-100">Submit</button>
                     <div class="row">
                        <div class="col-md-6"><button type="button" class="btn btn-primary w-100 form_btn" data-value="login">Back to Login</button></div>
                        <div class="col-md-6"><button type="button" class="btn btn-primary w-100 form_btn" data-value="signup">Sign Up</button></div>
                     </div>
                  </form>
                  <form name="signup" class="d-none" >
                     <div class="input-group mb-3">
                        <div class="input-group-prepend">
                           <span class="input-group-text" id="basic-addon1"><i class="material-icons">account_circle</i></span>
                        </div>
                        <input type="text" class="form-control" name="username" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" required>
                     </div>
                     <div class="input-group mb-4">
                        <div class="input-group-prepend">
                           <span class="input-group-text" id="basic-addon2"><i class="material-icons">lock</i></span>
                        </div>
                        <input type="password" class="form-control" name="password" placeholder="Password" aria-label="password" aria-describedby="basic-addon2" required>
                     </div>
                     <div class="input-group mb-3">
                        <div class="input-group-prepend">
                           <span class="input-group-text" id="basic-addon1"><i class="material-icons">alternate_email</i></span>
                        </div>
                        <input type="email" class="form-control" name="email" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1" required>
                     </div>
                     <button type="submit" class="btn btn-primary btn-raised w-100">Sign Up</button>
                     <div class="row">
                        <div class="col-md-12"><a href="" class="btn btn-primary w-100 form_btn" data-value="login">Back to Login</a></div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script type="text/javascript" src="{{env('BASE_URL')}}public/js/login.js"></script>
@include('footer')