@include('header')
<section>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-6 mx-auto">
            <div class="card">
               <div class="card-body">
                  <form name="add-collection">
                     <div class="form-group">
                        <label for="api_name">API Name</label>
                        <input type="text" class="form-control" name="api_name" id="api_name" placeholder="enter_api_name" required>
                     </div>
                     <div class="form-group">
                        <label for="endpoint">API URL</label>
                        <input type="text" class="form-control" name="endpoint" id="endpoint" placeholder="http://" required>
                     </div>
                     <label for="exampleInputEmail1">Method</label>
                     <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" name="method" id="post" class="custom-control-input" value="POST" checked required>
                           <label class="custom-control-label" for="post">POST</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" name="method" id="get" class="custom-control-input" value="GET" required>
                           <label class="custom-control-label" for="get">GET</label>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="params">Payload</label>
                        <textarea class="form-control" name="params" id="params" rows="10" spellcheck="false" placeholder="Rows are separated by new lines" style="font-size:13px;font-family:'Courier New', san-serif"></textarea>
                     </div>
                     <a href="{{ env('BASE_URL') }}view?t=1" class="btn btn-secondary">Back to list</a>
                     <button type="submit" class="btn btn-primary btn-raised">Create</button>
                     
                     <div class="modal fade" id="update-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                       <div class="modal-dialog" role="document">
                         <div class="modal-content">
                           <div class="modal-header">
                             <h5 class="modal-title" id="exampleModalLabel">Adding API</h5>
                           </div>
                           <div class="modal-body">
                             Are you sure you want to continue?
                           </div>
                           <div class="modal-footer">
                             <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                             <button type="submit" class="btn btn-primary btn-raised">Yes</button>
                           </div>
                         </div>
                       </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script type="text/javascript" src="{{env('BASE_URL')}}public/js/add-api.js"></script>
@include('footer')