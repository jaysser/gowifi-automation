@include('header')
<div class="modal" tabindex="-1" role="dialog" id="deleteCollection">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Deleting Collection</h5>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to continue?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-raised">Delete</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<section class="pb-0">
   <div class="container-fluid">
   </div>
</section>
<section>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-11 mx-auto">
         <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
               <a class="nav-link active" id="collection-tab" data-toggle="tab" href="#collection" role="tab" aria-controls="collection" aria-selected="false">Collection</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" id="api-tab" data-toggle="tab" href="#api" role="tab" aria-controls="api" aria-selected="true">API List</a>
            </li>
         </ul>
         <div class="tab-content" id="myTabContent">
            <div class="tab-pane active" id="collection" role="tabpanel" aria-labelledby="collection-tab">
               <div class="card">
                  <div class="card-body">                  
                     <div class="row">
                        <div class="col-md-12 mx-auto">
                           <a href="{{env('BASE_URL')}}add/collection" class="btn btn-primary btn-raised float-right mb-3">Create Collection</a>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <table id="collections" class="table">
                              <thead>
                                 <tr>
                                    <th width="40%">Name</th>
                                    <th width="20%">No. of APIs</th>
                                    <th width="20%">Date Created</th>
                                    <th width="5%">&nbsp;</th>
                                 </tr>
                              </thead>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane show" id="api" role="tabpanel" aria-labelledby="api-tab">
               <div class="card">
                  <div class="card-body">                  
                     <div class="row">
                        <div class="col-md-12 mx-auto">
                           <a href="{{env('BASE_URL')}}add/api" class="btn btn-primary btn-raised float-right mb-3">Create API</a>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <table id="apis" class="table">
                              <thead>
                                 <tr>
                                    <th width="40%">Name</th>
                                    <th width="20%">URL</th>
                                    <th width="20%">Method</th>
                                    <th width="20%">Date Created</th>
                                    <th width="5%">&nbsp;</th>
                                 </tr>
                              </thead>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
            
         </div>
      </div>
   </div>
</section>
@include('footer')
<script type="text/javascript" src="{{env('BASE_URL')}}public/js/view.js"></script>
<script type="text/javascript">
   var base_url = "{{env('BASE_URL')}}";
    (function(window, undefined){
        window.jQuery = window.$ = jQuery;
    })(window);
</script>
</style>
