<div class="modal fade" id="change_pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
         </div>
         <div class="modal-body pb-2">
            <form name="change_password">
               <input type="hidden" name="username" value="@php if(session('user_info') != null) echo current(array_keys(session('user_info'))); @endphp">
               <div class="form-group mb-3">
                  <label for="pass" class="bmd-label-static">Current Password</label>
                  <div class="input-group mb-3">
                     <input type="password" class="form-control classic pass_input" id="pass" pattern=".{5,16}" maxlength="16" name="password" aria-label="current_pass" aria-describedby="basic-addon2" required>
                     <div class="input-group-append">
                        <button tabindex="-1" class="btn classic toggle_pass" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                     </div>
                  </div>
               </div>
               <div class="form-group mb-3">
                  <label for="new_pass" class="bmd-label-static">New Password <span class="badge badge-light" data-toggle="tooltip" data-placement="right" title="Password should be 5-16 characters containing A-Z, a-z, 0-9 and/or special characters !@#$%^&*()_">?</span></label>
                  <div class="input-group mb-3">
                     <input type="password" class="form-control classic pass_input" id="new_pass" pattern=".{5,16}" maxlength="16" name="new_password" aria-label="new_pass" aria-describedby="basic-addon2" required>
                     <div class="input-group-append">
                        <button tabindex="-1" class="btn classic toggle_pass" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                     </div>
                  </div>
               </div>
               <div class="form-group mb-3">
                  <label for="confirm_pass" class="bmd-label-static">Confirm Password</label>
                  <div class="input-group mb-3">
                     <input type="password" class="form-control classic pass_input" id="confirm_pass" pattern=".{5,16}" maxlength="16" name="confirm_password" aria-label="confirm_pass" aria-describedby="basic-addon2" required>
                     <div class="input-group-append">
                        <button tabindex="-1" class="btn classic toggle_pass" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                     </div>
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group col-7 mb-0" id="errors"></div>
                  <div class="form-group col-5 mb-0 text-right">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                     <button type="submit" class="btn btn-primary btn-raised">Save</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{env('BASE_URL')}}public/js/jquery.filter_input.js"></script>
<script src="{{env('BASE_URL')}}public/js/change_password.js"></script>
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/js/dataTables.checkboxes.min.js"></script>
<script type="text/javascript">
   $('[data-toggle="tooltip"]').tooltip();
   $(document).find(' :input').on('change',function(){
      $(this).val($(this).val().trim());
   });
   var base_url = "{{env('BASE_URL')}}";
   toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-center",
      "preventDuplicates": true,
      "preventOpenDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };
   /* window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
      var error_msg = errorMsg.split("at position")[0];
       toastr.error(error_msg,"Error occured");
       return false;
   } */
   
   
   $('input[name=collection],input[name=filename],input[name=collection_name]').filter_input({regex:'[A-Za-z0-9_]'});
   $('.pass_input').filter_input({regex:'[A-Za-z0-9!@#$%^&*()_]'});
   
   function getParameterByName(name, url) {
       if (!url) url = window.location.href;
       name = name.replace(/[\[\]]/g, "\\$&");
       var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
           results = regex.exec(url);
       if (!results) return null;
       if (!results[2]) return '';
       return decodeURIComponent(results[2].replace(/\+/g, " "));
   }
   function validateUrl(url) {
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if (pattern.test(url)) {
            return true;
        } 
         return false;
 
    }
    function parseKeyPairtoJSON(content){
      var tempArray = new Array(),counter;
      content = content.replace(/\n/g,"&").split("&");
      for(counter=0;counter<content.length;counter++) {
         tempArray.push(content[counter].replace(/:(.*?)/,"="));
      } 
      return getQueryParams(tempArray.join("&"));
    }
    
   function getQueryParams (query) {
     return query.replace(/^\?/, '').split('&').reduce((json, item) => {
       if (item) {
         item = item.split('=').map((value) => decodeURIComponent(value))
         json[item[0].trim()] = item[1].trim()
       }
       return json
     }, {})
   }
</script>