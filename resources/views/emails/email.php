<?php 

$head = '<table cellpadding="0" cellspacing="0" align="center" width="100%" style="table-layout: fixed;">
   <tr>
      <td>
         <table cellpadding="0" cellspacing="0" align="center" width="100%">';

$footer = '</table>
      </td>
   </tr>
</table>';
         
$body = $head;

$i = 0;
var_dump($result);die();
foreach($result['success'][$i] as $key -> $value) {
   $api_name = $key;
   $http_code = $value['code'];
   $message = $value['message'];
   $payload = $value['payload'];
   
   if($value['code'] == 200) {
      $body .= success($api_name,$http_code,$message,$payload);
   }
   else {
      $response = $value['data'];
      $body .= failed($api_name,$http_code,$message,$payload,$response);
   }
   
   $i++;
}

$body .= $footer;


function success($api_name,$http_code,$message,$payload) {
   return '<tr>
               <td>
                  <table cellpadding="0" cellspacing="0" align="center" width="100%" style="margin-bottom:20px">
                     <tr>
                        <td>
                           <div style="background-color: #d4edda;padding: 10px;color: #155724;font-weight: bold;">
                              <p style="font-family:Arial,san-serif;font-size: 13px;margin:0">'. $api_name .'</p>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <div style="background-color: #fff;padding: 10px;color: #000;">
                              <table cellpadding="0" cellspacing="0" align="center" width="100%" style="table-layout: fixed;">
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Status:</b> '. $http_code .'</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Message:</b> '. $message .'</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Payload:</b><br/> 
                                       '. $payload .'</p>
                                    </td>
                                 </tr>
                              </table>
                           </div>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>';
}

function failed($api_name,$http_code,$message,$payload, $response) {
   return '<tr>
               <td>
                  <table cellpadding="0" cellspacing="0" align="center" width="100%" style="margin-bottom:20px">
                     <tr>
                        <td>
                           <div style="background-color:#f8d7da;padding:10px;color:#721c24;font-weight: bold;">
                              <p style="font-family:Arial,san-serif;font-size: 13px;margin:0">'. $api_name .'</p>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <div style="background-color: #fff;padding: 10px;color: #000;">
                              <table cellpadding="0" cellspacing="0" align="center" width="100%" style="table-layout: fixed;">
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Status:</b> '. $http_code .'</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Message:</b> '. $message .'</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Payload:</b><br/> 
                                       '. $payload .'</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Response:</b><br/> 
                                       '. $response .'</p>
                                    </td>
                                 </tr>
                              </table>
                           </div>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>';
}

?>