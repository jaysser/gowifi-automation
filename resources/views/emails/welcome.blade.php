<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>WELCOME - API TEST AUTOMATION</title>
</head>
<body>

<h2>Account has been created.</h2>
<p>
 Hello there!<br><br>

 Your account has been created. Please log in <a href="{{$website_login}}">here</a> with your credentials.<br><br>

 @if($username != '')
 <strong>Username:</strong> {{$username}}<br>
 @endif
 <strong>Password:</strong> {{$password}}<br><br>

 If you run into any trouble, please feel free to message us.
</body>
</html>