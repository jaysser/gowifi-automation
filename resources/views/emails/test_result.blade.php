<?php 
$head = '<table cellpadding="0" cellspacing="0" align="center" width="100%" style="table-layout: fixed;">
   <tr>
      <td>
         <table cellpadding="0" cellspacing="0" align="center" width="100%">';

$footer = '</table>
      </td>
   </tr>
</table>';
         
$body = $head; 

foreach($result as $key => $value) {
   if($key == "success"){
      foreach($result["success"] as $k_success => $v_success) {
         foreach($v_success as $k => $v) {
            $api_name = $k;
            $http_code = (isset($v->code)) ? $v->code : 'undefined';
            $message = (isset($v->message)) ? $v->message : '';
            $payload = (isset($v->payload)) ? json_encode($v->payload) : '';
            $response = (isset($v->data)) ? json_encode($v->data) : '';
            
            if($http_code == 200)
               $status = "success";
            elseif($http_code == 'undefined')
               //$status = "info";
               if($response != '') {
                  $http_code = 200;
               }
               else {
                  $http_code = "";
               }
            else
               $status = "danger";
            
            $body .= generateAlert($api_name,$http_code,$message,$payload,$response);
         }
      }
   }
   
   if($key == "failed"){
      $api_name = $result["failed"]['api'];
      $http_code = 500;
      $message_part = explode("response:",$result["failed"]['return']);
      $message = $message_part[0];
      $payload = json_encode($result["failed"]['payload']);
      $response = (isset($result["failed"]['data'])) ? json_encode($result["failed"]['data']) : '';
      $body .= generateAlert($api_name,$http_code,$message,$payload,$response);
   }
}
$body .= $footer;
print_r($body);
// $result = $body;

function generateAlert($api_name,$http_code,$message,$payload,$response) {
   
   if($http_code == 200) {
      $color = '#155724';
      $backgroundColor = '#d4edda';
   }
   elseif($http_code == 'undefined') {
      $color = '#383d41';
      $backgroundColor = '#e2e3e5';
      $http_code = '';
   }
   else {
      $color = '#721c24';
      $backgroundColor = '#f8d7da';
   }
   
   return '<tr>
               <td>
                  <table cellpadding="0" cellspacing="0" align="center" width="100%" style="margin-bottom:20px">
                     <tr>
                        <td>
                           <div style="background-color: '. $backgroundColor .';padding: 10px;color: '. $color .';font-weight: bold;">
                              <p style="font-family:Arial,san-serif;font-size: 13px;margin:0">'. $api_name .'</p>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <div style="background-color: #fff;padding: 10px;color: #000;">
                              <table cellpadding="0" cellspacing="0" align="center" width="100%" style="table-layout: fixed;">
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Status:</b> '. $http_code .'</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Message:</b> '. $message .'</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Payload:</b><br/> 
                                       '. $payload .'</p>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p style="word-wrap: break-word;font-family:\'Courier New\',san-serif;font-size: 13px;margin:0 0 5px"><b>Response:</b><br/> 
                                       '. $response .'</p>
                                    </td>
                                 </tr>
                              </table>
                           </div>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>';
}


?>