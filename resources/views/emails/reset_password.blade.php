<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>Forgot Password</title>
</head>
<body>

<h2>Forgot Password?</h2>
<p>
 We got you!<br><br>

 The password for your account has been reset. Please log in <a href="{{$website_login}}">here</a> with your new credentials.<br><br>

 @if($username != '')
 <strong>Username:</strong> {{$username}}<br>
 @endif
 <strong>Password:</strong> {{$password}}<br><br>

 Once logged in, you may change your temporary password. <br><br>

 If you run into any trouble, please feel free to message us.
</body>
</html>