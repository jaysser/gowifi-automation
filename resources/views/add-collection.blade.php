@include('header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<style>
.form-group > label {
   margin-bottom: 10px !important;
}
.btn.disabled:hover {
    cursor: not-allowed;
    color: rgba(0,0,0,.26);
}
</style>
<section class="">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-5 mx-auto mt-5" id="collection_name_form">
            <div class="card">
               <div class="card-body">
                  <form name="add-collection-name">
                     <div class="form-group">
                        <label for="collection_name">Collection Name</label>
                        <input type="text" class="form-control classic" name="collection_name" placeholder="enter_collection_name" required>
                     </div>
                     <div class="text-right">
                        <a href="{{ env('BASE_URL') }}view" class="btn btn-secondary">Back to list</a>
                        <button type="submit" name="create" class="btn btn-primary btn-raised">Create</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         
         <div class="col-md-8 mx-auto d-none" id="collection_det_form">
            <div class="card" id="apis">
               <form name="add_collection" action="" method="post">
                  <input type="hidden" name="collection_name" value="">
                  <h5 class="card-header">Collection Name</h5>
                  <div class="card-body">
                     <div name="add-collection">
                     </div>
                  
                  </div>
                  <div class="card-footer">
                     <div class="form-group text-right mb-2">
                        <a href="{{ env('BASE_URL') }}view" class="btn btn-secondary">Back to List</a>
                        <button type="submit" name="save" class="btn btn-primary btn-raised">Save</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>

<div class="modal fade" id="update-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adding Collection</h5>
      </div>
      <div class="modal-body">
        Are you sure you want to continue?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary btn-raised">Yes</button>
      </div>
    </div>
  </div>
</div>
<!-- <script type="text/javascript" src="{{env('BASE_URL')}}public/js/add.js"></script> -->
<script>
$.fn.extend({
  animateCss: function(animationName, callback) {
    var animationEnd = (function(el) {
      var animations = {
        animation: 'animationend',
        OAnimation: 'oAnimationEnd',
        MozAnimation: 'mozAnimationEnd',
        WebkitAnimation: 'webkitAnimationEnd',
      };

      for (var t in animations) {
        if (el.style[t] !== undefined) {
          return animations[t];
        }
      }
    })(document.createElement('div'));

    this.addClass('animated ' + animationName).one(animationEnd, function() {
      $(this).removeClass('animated ' + animationName);

      if (typeof callback === 'function') callback();
    });

    return this;
  },
});

$('form[name=add-collection-name]').on('submit',function(e){
   e.preventDefault();
   value = $(this).find('input[name=collection_name]').val();
   $('form[name=add_collection] input[name=collection_name]').val(value);
   $('div[name=add-collection]').append(generateAddForm('1'));
   if(value != '' ) {
      $('#collection_name_form').animateCss('fadeOutUp',function(){
         $('#collection_name_form').hide();
         $('#collection_det_form').removeClass('d-none');
         $('#collection_det_form .card-header').text($('input[name=collection_name]').val());
         $('#collection_det_form').animateCss('fadeInUp');
      });
   }
});

$(document).off('click','button');
$(document).unbind('click','button');
$('div[name=add-collection]').on('click', 'button[name=add]',function(){
   var parent_form = $('div[name=add-collection]'), total_forms = parseInt($('.api-form:last-child').attr('id').substr(4)) + 1;
   cloned_element = generateAddForm(total_forms);
   $(parent_form).append($(cloned_element));
   $(this).hide();
   $('button[name=remove]').removeClass('disabled');
});

$('div[name=add-collection]').on('click','button[name=remove]',function(){
   if($('.api-form').length != 1) {
      var this_element = $(this).parents(".api-form");
      $(this_element).prev('hr').remove();
      $.when($(this_element).remove()).then(function(){
         if($('.api-form').length == 1) {
            $('button[name=remove]').addClass('disabled');
            $('button[name=add]').show()
         }
      });
   }
});

$('form[name=add_collection]').submit(function(e){
   e.preventDefault();
   var api_value, form_json = [], api_json = temp_json = {}, counter = 0;
   filename = $(this).find('input[name=collection_name]').val();
   $('.api-form :input:not(button)').val(function(){return $(this).val().trim();});
   $('.api-form').each(function(){
      _this = $(this).attr('id');
      _this_param = {
         api: $(this).find('input[name=api_name]').val(),
         params: parseKeyPairtoJSON($(this).find('textarea').val()),
         path: {
            production: $(this).find('input[name=production]').val(),
            staging: $(this).find('input[name=staging]').val()
         }
      };
      $.extend(api_json,_this_param);
      api_value = $("#"+_this).find(':input[type=radio]').serialize();
      api_value = JSON.parse('{"' + decodeURI(api_value).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
      $.extend(api_json,api_value);
      
      form_json.push(api_json);
      api_json = {};
      
   });
   data = {
      filename: filename,
      data: JSON.stringify(form_json)
   }
   
   $.ajax({
      url: base_url+"api/file/create",
      data: JSON.stringify(data),
      type: "post",
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      success: function(data){
         if(typeof(Storage) !== "undefined") {
            sessionStorage.setItem("adding_collection_msg",data.message);
            window.location.replace(base_url+'view');
         }
         else {
            toastr.success(data.message,'',{onHidden:function(){window.location.replace(base_url+'view')}});
         }
      },
      error: function(response) {
         console.log(response)
         lastToastr = (response.responseJSON) ? toastr['error'](response.responseJSON.message) : toastr['error'](response.statusText, "Error: "+response.status);
      }
   });
   
   //console.log(form_json);
});


function generateAddForm(id) {
return `
<div class="row api-form px-2" id="api_`+id+`">
   <div class="col-md-5 my-4"> 
      <div class="form-group mb-2">
         <label for="api_name_`+id+`">API Name</label>
         <input type="text" class="form-control classic api_name" name="api_name" id="api_name_`+id+`" required>
      </div>
      <div class="form-group mb-2">
         <label for="st_url_`+id+`">Staging URL</label>
         <input type="text" class="form-control classic staging" name="staging" id="st_url_`+id+`" required>
      </div>
      <div class="form-group mb-3">
         <label for="pd_url_`+id+`">Production URL</label>
         <input type="text" class="form-control classic prod" name="production" id="pd_url_`+id+`" required>
      </div>
   </div>
   <div class="col-md-7 my-4"> 
      <div class="form-group mb-2">
         <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" name="method_`+id+`" id="post_`+id+`" class="custom-control-input method" value="POST" checked required>
            <label class="custom-control-label" for="post_`+id+`">POST</label>
         </div>
         <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" name="method_`+id+`" id="get_`+id+`" class="custom-control-input method" value="GET">
            <label class="custom-control-label" for="get_`+id+`">GET</label>
         </div>
      </div>
      <div class="form-group">
         <label for="params_`+id+`">Parameters</label>
         <textarea class="form-control classic params" name="params" rows="10" spellcheck="false" placeholder="Key_1: value 1
Key_2: {index : value}" style="font-size:13px;font-family:'Courier New', san-serif"></textarea>
      </div>
      <div class="form-group text-right mb-2">
         <button type="button" name="remove" class="btn btn-warning disabled"><i class="fa fa-times" aria-hidden="true"></i> Remove</button>
         <button type="button" name="add" class="btn btn-primary btn-raised"><i class="fa fa-plus" aria-hidden="true"></i> Add API</button>
      </div>
   </div>
</div>
`;
}

/* window.onbeforeunload = function(){
   return "Changes you made may not be saved.";
} */
</script>
@include('footer')