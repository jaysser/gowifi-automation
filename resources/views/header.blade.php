<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/png" sizes="96x96" href="../favicon-96x96.png">
<link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css"/>
<link rel="stylesheet" href="{{ env('BASE_URL') }}public/css/style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
<title>API Test Automation</title>
</head>
<body>
@if(Session::get('user_info'))
   
   @if(current(array_keys(session('user_info'))) == "admin")

<nav class="navbar sticky-top navbar-expand-lg navbar-dark">
   <div class="container-fluid">
      <a class="navbar-brand" href="{{ env('BASE_URL') }}"><img id='logo' src="{{ env('BASE_URL') }}resources/assets/wifi.png"> <span style="color:#F9F9FA">GO</span><span style="color:#FCFC0E">Wifi</span> API Test Automation</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
         <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link active" id="logout" href="{{ env('BASE_URL') }}logout">Log out</a>
            </li>
          </ul>
      </div>
   </div>
</nav>

   @else
         
<nav class="navbar sticky-top navbar-expand-lg navbar-dark">
   <div class="container-fluid">
      <a class="navbar-brand" href="{{ env('BASE_URL') }}"><img id='logo' src="{{ env('BASE_URL') }}resources/assets/wifi.png"> <span style="color:#F9F9FA">GO</span><span style="color:#FCFC0E">Wifi</span> API Test Automation</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
         <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link active" id="view" href="{{ env('BASE_URL') }}view">Library</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="run" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Run Automation
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="{{ env('BASE_URL') }}run/api">API Test</a>
                <a class="dropdown-item" href="{{ env('BASE_URL') }}run/collection">Collection Test</a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="quick" href="{{ env('BASE_URL') }}run/quick">Quick Test</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Account
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"id="account">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change_pass" data-keyboard="false" data-backdrop="static">Change Password</a>
                <a class="dropdown-item" href="{{ env('BASE_URL') }}logout">Log out</a>
              </div>
            </li>
          </ul>
      </div>
   </div>
</nav>

   @endif
@endif
