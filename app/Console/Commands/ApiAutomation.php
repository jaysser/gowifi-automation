<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Api\AutomationController as Automation;
use DB;
use Mail;

class ApiAutomation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'CheckOperationDay:checkoperationday';

    protected $signature = 'ApiAutomation:apiautomation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run daily automation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
        * Run collection test automation
        * @params 
        * - param 1 : collection_name string
        * - param 2 : default array
        * - param 3 : email_address string
        * - param 4 : staging/prod (0/1) int
        * - param 5 : username string
        */
        
        $result     = array();
        $automation = new Automation();

        //just add new line for another test of collection
        $result[]   = $automation->runApiCollection('go_wifi_auto',array(),'jbalido@yondu.com',0,'gowifi');

        file_put_contents(storage_path('logs/schedule-call-collection_automation-'.date('Ymd').'.log'), date('Y-m-d H:i:s').' -- '.json_encode($result).PHP_EOL, FILE_APPEND);

        print_r($result);
    }
}
