<?php

namespace App\Services;

use App\User;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use Mail;

class MailService
{
    public static function sendWelcomeUserEmail($username,$password,$to){
        Mail::send('emails.welcome', ['username' => $username,'password' => $password,'website_login' => env('BASE_URL')], function ($message) use ($to) {
            $message->from('no-reply@mobile360.ph', 'Welcome - API Test Automation')
                ->to($to)
                ->subject("Account has been created.");
        });

        return Mail::failures();        
    }

    public static function sendResultsViaEmail($to,$collection,$result){

        Mail::send('emails.test_result', ['result' => $result], function ($message) use ($collection,$to) {
            $message->from('no-reply@mobile360.ph', 'API Test Automation')
                ->to($to)
                ->subject("API Test Automation Result : ".$collection);
        });

        return Mail::failures();
    }

    public static function sendNewPasswordViaEmail($to,$username,$password){
    	Mail::send('emails.reset_password', ['username' => $username,'password' => $password,'website_login' => env('BASE_URL')], function ($message) use ($to) {
            $message->from('no-reply@mobile360.ph', 'API Test Automation - Password Reset')
                ->to($to)
                ->subject("Password has been reset.");
        });

        if(Mail::failures())
        	return false;
        else
        	return true;	
    }
}