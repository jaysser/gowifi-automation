<?php

namespace App\Services;

use App\User;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;

class FileService
{
    protected static $session;
    protected static $username;

    public static function setSession($session){
        self::$session = $session;
    }

    public static function setUsername($username){
        self::$username = $username;
    }

    public static function getSession(){
        return self::$session;
    }

    public static function parseFile($filename,$path=0){    
        $result   = null;

        if($path == 0){
            if(file_exists(base_path().'/resources/assets/config/'.self::$username.'/'.$filename.'.json'))
                $result = file_get_contents(env('BASE_URL')."resources/assets/config/".self::$username."/{$filename}.json");
        }else{
            if(file_exists(base_path().'/resources/assets/config/'.self::$username.'/api/'.$filename.'.json'))
                $result = file_get_contents(env('BASE_URL')."resources/assets/config/".self::$username."/api/{$filename}.json");
        }

        if($result !== null)
            return json_decode($result,true);
        else
            return $result;
    }

    public static function createFile($filename,$data,$path=0){
        $result   = false;
        $filename = str_replace(" ", "_", strtolower($filename)); 

        if($path == 0){
            $file   = base_path().'/resources/assets/config/'.self::$username.'/'.$filename.'.json';
            if(!file_exists($file)){
                $result = file_put_contents($file, $data);
            }
        }else{
            $file   = base_path().'/resources/assets/config/'.self::$username.'/api/'.$filename.'.json';
            if(!file_exists($file)){
                $result = file_put_contents($file, $data);
            }
        }

        return $result;
    }

    public static function deleteFile($filename,$path="false"){
        $result = false;

        if($path == "false"){
            $file = base_path().'/resources/assets/config/'.self::$username.'/'.$filename.'.json';
            if(file_exists($file))
                $result = unlink($file);
        }else{
            $file = base_path().'/resources/assets/config/'.self::$username.'/api/'.$filename.'.json';
            if(file_exists($file))
                $result = unlink($file);
        }

        return $result;
    }

    public static function updateFile($filename,$data,$newName = false){
        $file    = base_path().'/resources/assets/config/'.self::$username.'/'.$filename.'.json';
        $result  = false;
        
        if($newName !== false)
            $newName = str_replace(" ", "_", strtolower($newName));

        if($newName !== false && !file_exists(base_path().'/resources/assets/config/'.self::$username.'/'.$newName.'.json')){
            rename($file, base_path().'/resources/assets/config/'.self::$username.'/'.$newName.'.json');
            $result = file_put_contents(base_path().'/resources/assets/config/'.self::$username.'/'.$newName.'.json', $data);
        }elseif($newName !== false && file_exists(base_path().'/resources/assets/config/'.self::$username.'/'.$newName.'.json')){
            $result = false;
        }else{
            if(file_exists($file))
                $result = file_put_contents($file, $data);
        }

        return $result;
    }

    public static function updateApifile($api,$data){
        $file   = base_path().'/resources/assets/config/'.self::$username.'/api/'.$api.'.json';
        $result = false;
        $array  = json_decode($data,true);

        if(count($array) > 0){
            if(file_exists($file)){
                $result = file_put_contents($file, $data);
                if($array[0]['api'] != $api)
                    rename($file, base_path().'/resources/assets/config/'.self::$username.'/api/'.$array[0]['api'].'.json');
            }
        }

        return $result;
    }

    public static function getApiList($username = ''){
        if($username == ''){
            $username = self::$username;
        }

        $scanned_directory = array_values(array_diff(scandir(base_path().'/resources/assets/config/'.$username.'/api'), array('..', '.')));

        $result = array();

        foreach ($scanned_directory as $key => $value) {
            $file     = base_path().'/resources/assets/config/'.$username.'/api/'.$value;
            $filename = str_replace(".json", "", $value);

            $file_content                 = self::parseFile($filename,1);
            $result[$key]['date_created'] = date("Y-m-d H:i:s", filectime($file));  
            $result[$key]['filename']     = $filename;
            $result[$key]['path']         = $file_content[0]['path'];
            $result[$key]['method']       = $file_content[0]['method'];
            $result[$key]['title']        = ucwords(str_replace("_"," ",str_replace(".json", "", $value)));
        }
        return $result;
    }

    public static function getCollectionList($username=''){
        if($username == ''){
            $username = self::$username;
        }

    	$scanned_directory = array_values(array_diff(scandir(base_path().'/resources/assets/config/'.$username.'/'), array('api','..', '.')));
    	$result = array();

    	foreach ($scanned_directory as $key => $value) {
            $file     = base_path().'/resources/assets/config/'.$username.'/'.$value;
            $filename = str_replace(".json", "", $value);

            $file_content                 = self::parseFile($filename);
            $result[$key]['date_created'] = date("Y-m-d H:i:s", filectime($file));  
    		$result[$key]['filename']     = str_replace(".json", "", $value);
    		$result[$key]['title']        = ucwords(str_replace("_"," ",str_replace(".json", "", $value)));
            $result[$key]['no_api']        = count($file_content);
    	}

    	return $result;
    }
}