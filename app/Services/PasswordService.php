<?php

namespace App\Services;

use App\User;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use App\Services\MailService as Mail;

class PasswordService
{
    public static function resetMultiplePassword($user){
        $result     = array();
        $file       = base_path().'/resources/assets/user.json';
        $userConfig = json_decode(file_get_contents($file),true);
        
        foreach ($user as $key => $username) {
            $email      = '';
            $password   = self::randomString(8);

            foreach ($userConfig as $k => &$val) {
                if(array_key_exists($username, $val)){
                    $val[$username]['password'] = bcrypt($password);
                    $email = $val[$username]['email'];
                }
            }

            //apply changes
            file_put_contents($file, json_encode($userConfig));
            $result[] = Mail::sendNewPasswordViaEmail($email,$username,$password);
        }
        
        return $result;
    }

	public static function resetPassword($username,$email){
		$file       = base_path().'/resources/assets/user.json';
        $userConfig = json_decode(file_get_contents($file),true);
        $password   = self::randomString(8);

        foreach ($userConfig as $key => &$value) {
        	if(array_key_exists($username, $value)){
        		$value[$username]['password'] = bcrypt($password);
        	}
        }

        //apply changes
        file_put_contents($file, json_encode($userConfig));
        $result = Mail::sendNewPasswordViaEmail($email,$username,$password);
        
        return $result;
	}

    public static function changePassword($username,$password,$new_password){
        $result     = array();
        $file       = base_path().'/resources/assets/user.json';
        $userConfig = json_decode(file_get_contents($file),true);

        foreach ($userConfig as $key => &$value) {
            if(array_key_exists($username, $value)){
                if(password_verify($password,$value[$username]['password'])){
                    $value[$username]['password'] = bcrypt($new_password);
                }
                else{
                    $result['error']   = true;
                    $result['message'] = "Incorrect current password.";
                    return $result;    
                }
            }
        }

        $result = file_put_contents($file, json_encode($userConfig));
        return $result;
    }

	public static function randomString($len)
    {
        $string = "";
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for($i=0;$i<$len;$i++)
            $string.=substr($chars,rand(0,strlen($chars)),1);

        if(strlen($string) < $len || preg_match('~[0-9]~', $string) < 1)
            $string = self::randomString($len);
           
        return $string;
    }
}