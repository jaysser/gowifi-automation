<?php

namespace App\Services;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use Illuminate\Support\Facades\Session;

class CollectionService
{
    public static function runCollection($data,$data_ui=array(),$env=0){
    	$return  = array();
		$global  = array();

    	if($data !== null){
         $i = 0;
    		foreach ($data as $key => $value) {
    			$response = '';
    			$query    = self::queryBuilder($value['params']) != '' ? '?':'';

    			$params   = self::getActualParams($global,$value['params'],$value['api'],$data_ui);
				$query   .= (strtoupper($value['method']) == "GET" ? self::queryBuilder($params):'');
				$response = self::callApi($value['path'],$query,$value['method'],$params);

				if(is_object($response)){
					$global = array_merge($global,self::setGlobalVariable($response,$data_ui,$value['params']));
					$return['success'][$i][$value['api']] = $response;
               		$return['success'][$i][$value['api']]->payload = (strtoupper($value['method']) == "POST" ? $params:$query);
               		$return['success'][$i][$value['api']]->end_point = $value['path'];

                    if(isset($response->code) && $response->code >= 400)
                        return $return;
				}else{
					$return['has_error'] = true;
					$return['failed']['api']          = $value['api'];
					$return['failed']['payload']      = (strtoupper($value['method']) == "POST" ? $params:$query);
					$return['failed']['return']       = isset($response['message']) ? $response['message']:'';
					$return['failed']['end_point']    = $value['path'];
                    $return['code']                   = isset($response['code']) ? $response['code']:500;
                    $return['message']                = isset($response['message']) ? $response['message']:"Internal Server Error";

					return $return;	
				}
            $i++;
    		}
    	}

    	return $return;
    }

    private static function setGlobalVariable($response,$data_ui,$params){
    	$global = array();

    	if(count($data_ui) > 0){
    		foreach ($data_ui as $key => $value) {
    			$global[$key] = $value;
    		}
    	}

    	if(is_object($response)){
    		if(isset($response->data)){
	    		foreach ($response->data as $key => $value) {
	    			if($key == 'telco_id')
	    				$global['telco'] = $value;

	    			if($key == 'username'){
	    				$global['msisdn'] = $value;
	    				$global['mobile_no'] = $value;
	    			}

	    			if($key == 'one_time_code')
	    				$global['code'] = $value; 

	    			if($key == 'device'){
	    				$global['device_id']   = isset($value->device_id) ? $value->device_id:'';
	    				$global['mac_address'] = isset($value->mac_address) ? $value->mac_address:$global['mac_address'];
	    				$global['serial']	   = isset($value->mac_address) ? $value->mac_address:$global['mac_address'];
	    			}

	    			if($key == 'current_plan')
	    			{
	    				if(isset($value->wifi_plan)){
	    					$global['serial_no'] = $value->wifi_plan->serial_no;
	    				}
	    			}

    				if(!isset($global[$key]))
    					$global[$key] = $value;
	    		}
    		}
    	}

    	return $global;
    }

    private static function getActualParams($global,$params,$api,$data_ui){
    	$realParams = array();

    	if(count($data_ui) > 0){
	    	foreach ($data_ui as $key => $value) {
	    		$global[$key] = $value;
	    	}
    	}

    	foreach ($global as $key => $value) {
    		if(array_key_exists($key, $params)){
    			$realParams[$key] = $value;
    		}
    	}

    	foreach ($params as $key => $value) {
    		if(!array_key_exists($key, $realParams))
    			$realParams[$key] = $value;

    		//get friend's msisdn from ui if set, else from config
    		if($api == "request_fi"){
    			if($key == "msisdn"){
    				if(count($data_ui) > 0){
    					$msisdn = substr($data_ui['request_fi_msisdn'], -10);

    					if(is_numeric($msisdn))
    						$realParams['msisdn'] = $msisdn;
    					else{
    						if(array_key_exists($key, $realParams))
		    					$realParams['msisdn'] = $value;
    					}
    				}else{
		    			if(array_key_exists($key, $realParams))
		    				$realParams['msisdn'] = $value;
    				}
    			}
    		}
    	}

    	return $realParams;
    }

    private static function callApi($path,$query,$method,$data = array(),$env=0){
    	try{    		
    		if(strtoupper($method) == "POST"){
    			$client = new Client();
                if($env == 0)
                    $path = $path['staging'];
                else
                    $path = $path['production'];

    			$result = $client->post($path, [
                	'form_params' => $data,
	                'headers' => [
	                    'Accept'     => 'application/json',
	                ]
            	]);

            	$apiResult  = json_decode((string) $result->getBody());
	    		return $apiResult;
    		}

    		if(strtoupper($method) == "GET"){
    			$client = new Client();
    			if($env == 0){
	    			$result = $client->get(
			            $path['staging'].$query, [
			            'headers' => [
			                'Accept'     => 'application/json',
			            ]
			        ]);
    			}elseif($env == 1){
    				$result = $client->get(
			            $path['production'].$query, [
			            'headers' => [
			                'Accept'     => 'application/json',
			            ]
			        ]);
    			}

		    	$apiResult  = json_decode((string) $result->getBody());
	    		return $apiResult;
    		}
		}catch (\Exception  $e) {
		    $result = array();
            $result['code']    = $e->getCode();
            $result['message'] = $e->getMessage();
            return $result;
		}
    }

    private static function queryBuilder($array){
        if(count($array) > 0){
        	$lastElement = end($array);
        	$query       = '';
        	foreach ($array as $key => $value) {
    			$query .= $key.'='.$value.($value != $lastElement ? '&':'');
        	}

        	return $query;
        }else{
            return '';
        }
    }
}