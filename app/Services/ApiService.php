<?php

namespace App\Services;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;

class ApiService    
{
    public static function runApi($data,$data_ui=array(),$env=0){
    	$return  = array();
        $i       = 0;

        if(null !== $data){
            foreach ($data as $key => $value) {
                $response = '';
                $query    = self::queryBuilder($value['params']) != '' ? '?':'';
                $params   = self::getActualParams($value['params'],$data_ui);
                $query   .= (strtoupper($value['method']) == "GET" ? self::queryBuilder($params):'');
                $response = self::callApi($value['path'],$query,$value['method'],$params);

                if(is_object($response)){
                    $return['success'][$i][$value['api']] = $response;
                    $return['success'][$i][$value['api']]->payload = (strtoupper($value['method']) == "POST" ? $params:$query);
                    $return['success'][$i][$value['api']]->end_point = $value['path'];
                }else{
                    $return['has_error'] = true;
                    $return['failed']['api']          = $value['api'];
                    $return['failed']['payload']      = (strtoupper($value['method']) == "POST" ? $params:$query);
                    $return['failed']['return']       = isset($response['message']) ? $response['message']:'';
                    $return['failed']['end_point']    = $value['path'];
                    $return['code']                   = isset($response['code']) ? $response['code']:500;
                    $return['message']                = isset($response['message']) ? $response['message']:"Internal Server Error"; 

                    return $return; 
                }
                $i++;
            }
        }
    	return $return;
    }

    private static function getActualParams($params,$data_ui){
        $realParams = array();

        if(count($data_ui) > 0){
            foreach ($data_ui as $key => $value) {
                $realParams[$key] = $value;
            }
        }else{
            foreach ($params as $key => $value) {
                $realParams[$key] = $value;
            }
        }

        return $realParams;
    }

    private static function queryBuilder($array){
        if(count($array) > 0){
            $lastElement = end($array);
            $query       = '';
            foreach ($array as $key => $value) {
                $query .= $key.'='.$value.($value != $lastElement ? '&':'');
            }

            return $query;
        }else{
            return '';
        }
    }

    private static function callApi($path,$query,$method,$data = array(),$env=0){
        try{            
            if(strtoupper($method) == "POST"){
                $client = new Client();
                $result = $client->post($path, [
                    'form_params' => $data,
                    'headers' => [
                        'Accept'     => 'application/json',
                    ]
                ]);

                $apiResult  = json_decode((string) $result->getBody());
                return $apiResult;
            }

            if(strtoupper($method) == "GET"){
                $client = new Client();
                $result = $client->get($path.$query, [
                    'headers' => [
                        'Accept'     => 'application/json',
                    ]
                ]);

                $apiResult  = json_decode((string) $result->getBody());
                return $apiResult;
            }
        }catch (\Exception $e) {
            $result = array();
            $result['code']    = $e->getCode();
            $result['message'] = $e->getMessage();
            return $result;
            // $response = $e->getResponse()->getBody(true);
            // return json_decode($response->getContents(),true);
        }
    }
}