<?php

namespace App\Services;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use App\Services\FileService as File;
use App\Services\PasswordService as Password;
use App\Services\MailService as Mail;

class UserService    
{   
    public static function getUserList(){
        $result     = array();
        $file       = base_path().'/resources/assets/user.json';
        $userConfig = json_decode(file_get_contents($file),true);

        foreach ($userConfig as $key => $value) {
            foreach ($value as $username => $val) {
                if($username !== 'admin'){
                    $result[$key]['username']   = $username;
                    $result[$key]['email']      = $val['email'];
                    $result[$key]['collection'] = count(File::getCollectionList($username));
                    $result[$key]['api']        = count(File::getApiList($username));
                }
            }
        }

        return array_values($result);
    }

    public static function getUser($username,$password){
        $result     = array();
        $file       = base_path().'/resources/assets/user.json';
        $userConfig = json_decode(file_get_contents($file),true);

        foreach ($userConfig as $key => $value) {
            if(array_key_exists($username, $value)){
                if(password_verify($password,$value[$username]['password'])){
                    $result = $value;
                    return $result;
                }
                else{
                    $result['error'] = true;
                    $result['message'] = "Invalid Password!";
                    return $result;    
                }
            }else{
                $result['error'] = true;
                $result['message'] = "Username doesn't exist!";
            }
        }

        return $result;                
    }

    public static function getUserByEmail($email){
        $file       = base_path().'/resources/assets/user.json';
        $userConfig = json_decode(file_get_contents($file),true);

        foreach ($userConfig as $key => $value) {
            foreach ($value as $k => $v) {
                if($v["email"] == $email)
                    return $value;
            }
        }
        return false;
    }

    public static function rmdir_recursive($dir) {
        foreach(scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$dir/$file")) self::rmdir_recursive("$dir/$file");
            else unlink("$dir/$file");
        }
        rmdir($dir);
    }

    public static function deleteMultipleAccounts($user){
        $result     = array();
        $file       = base_path().'/resources/assets/user.json';
        $userConfig = json_decode(file_get_contents($file),true);

        foreach ($user as $key => $username) {
            foreach ($userConfig as $k => &$val) {
                if(array_key_exists($username, $val)){
                    unset($val[$username]);
                    self::rmdir_recursive(base_path().'/resources/assets/config/'.$username);
                }
            }
        }

        //apply changes
        $result = file_put_contents($file, json_encode($userConfig));
        return $result;
    }

    public static function createUser($username,$password='',$email){
        if($password == ''){
            $password = Password::randomString(8);
        }

        $result     = array();
        $file       = base_path().'/resources/assets/user.json';
        $userConfig = json_decode(file_get_contents($file),true);

        try{

            foreach ($userConfig as $key => $value) {
                //check if username exist
                if(array_key_exists($username, $value)){
                    $result['error'] = true;
                    $result['message'] = "Username already exist!";
                    return $result;
                }

                //check if email_address exist
                foreach ($value as $k => $v) {
                    if($v["email"] == $email){
                        $result['error'] = true;
                        $result['message'] = "Email address already exist!";
                        return $result;   
                    }
                }
            }

            $data = array();
            $data[$username]['password'] = bcrypt($password);
            $data[$username]['email']    = $email;
            array_push($userConfig, $data);

            //update content
            $result      = file_put_contents($file, json_encode($userConfig));
            $user_folder = base_path().'/resources/assets/config/'.strtolower($username);
            $api_folder  = base_path().'/resources/assets/config/'.strtolower($username).'/api'; 

            //create folder and subfolder for the new user
            mkdir($user_folder);
            chmod($user_folder, 0777);

            mkdir($api_folder);
            chmod($api_folder, 0777);
            
            Mail::sendWelcomeUserEmail($username,$password,$email);

            return $result;
        }catch(\Exception $e){
            throw new \Exception($e);
        }

    }
}