<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Services\UserService as User;
use Illuminate\Http\Request;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function loginPage(Request $request)
    {
        if($request->session()->has('user_info')){
           if(current(array_keys($request->session()->get('user_info'))) == "admin"){
              return view('admin');
           }
           else{
              return view('view-collection');
           }
        }else{
            return view('login');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        \Cache::flush();
        //redirect
        return redirect()->action('Auth\LoginController@loginPage');
    }

    public function login(Request $request)
    {
        $response  = array();
        $validator = Validator::make(Input::all(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $response['code']    = 400;
            $response['message'] = $validator->messages()->all();
            return response()->json($response,400);
        }

        $username = $request->get('username');
        $password = $request->get('password');
        $user     = User::getUser($username,$password);

        try{
            if($user != false && !isset($user['error'])){
                Session::put('user_info', $user);
                $response['message']           = "Login Success";
                $response['code']              = 200;
                $response['data']['username']  = $username;
                Session::save();

                // return redirect('/');
                return response()->json($response,200);
            }else{
                $response['message'] = $user['message'];
                $response['code']  = 400;

                return response()->json($response,400);
            }
        }catch(\Exception $e){
            throw new \Exception($e);
        }
    }
}
