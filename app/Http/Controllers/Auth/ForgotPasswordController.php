<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Services\UserService as User;
use App\Services\PasswordService as Password;
use Validator;
use Illuminate\Support\Facades\Input;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function forgotPassword(Request $request)
    {
        $response  = array();
        $validator = Validator::make(Input::all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            $response['code']    = 400;
            $response['message'] = $validator->messages()->all();
            return response()->json($response,400);
        }

        $email    = $request->get('email');
        $user     = User::getUserByEmail($email);
        $result   = false;  
        
        if($user !== false){
            $result = Password::resetPassword(current(array_keys($user)),$email);
            if($result !== false){
                $response['status']  = 202;
                $response['message'] = "Password has been reset. Please check your email.";
                return response()->json($response,202);    
            }
        }else{
            $response['status']  = 404;
            $response['message'] = "Email address not found.";
            return response()->json($response,404);
        }
    }
}
