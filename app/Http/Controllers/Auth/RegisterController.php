<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function createUser(Request $request){
        $response  = array();
        $validator = Validator::make(Input::all(), [
            'username' => 'required',
            'email'    => 'required'
        ]);

        if ($validator->fails()) {
            $response['code']    = 400;
            $response['message'] = $validator->messages()->all();
            return response()->json($response,400);
        }
        
        $username = $request->get('username');
        $password = $request->get('password') ? $request->get('password'):'';
        $email    = $request->get('email');

        $result   = UserService::createUser($username,$password,$email);

        if($result !== false && !isset($result['error'])){
            $response['code']  = 201;
            $response['message'] = "User has been created!";
            return response()->json($response,201);
        }else{
            $response['code']  = 400;
            $response['message'] = $result['message'];
            return response()->json($response,400);
        }
    }
}
