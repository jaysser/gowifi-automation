<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Services\PasswordService as Password;
use Validator;
use Illuminate\Support\Facades\Input;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function changePassword(Request $request)
    {
        $response  = array();
        $validator = Validator::make(Input::all(), [
            'username'     => 'required',
            'password'     => 'required',
            'new_password' => 'required'
        ]);

        if ($validator->fails()) {
            $response['code']    = 400;
            $response['message'] = $validator->messages()->all();
            return response()->json($response,400);
        }

        $result   = array();
        $username = $request->get('username');
        $password = $request->get('password');
        $new_pass = $request->get('new_password');
        $result   = Password::changePassword($username,$password,$new_pass);

        if(!isset($result['error'])){
            $response['status']  = 202;
            $response['message'] = "Password has been updated.";
            return response()->json($response,202);
        }else{
            $response['status']  = 400;
            $response['message'] = $result['message'];
            return response()->json($response,400);
        }
    }

    public function resetPassword(Request $request){
        $response  = array();
        $validator = Validator::make(Input::all(), [
            'username'     => 'required',
        ]);

        if ($validator->fails()) {
            $response['code']    = 400;
            $response['message'] = $validator->messages()->all();
            return response()->json($response,400);
        }
        
        $username = $request->get('username'); //this is array
        $result   = array();
        $result   = Password::resetMultiplePassword($username);

        if(count($result) > 0){
            $response['code']    = 200;
            $response['message'] = "Password has been reset.";
            return response()->json($response,200); 
        }else{
            $response['code']    = 404;
            $response['message'] = "User not found";
            return response()->json($response,404);
        }
    }
}
