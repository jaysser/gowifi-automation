<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService as User;

class UserController extends Controller
{
    public function deleteAccount(Request $request)
    {
        $user     = $request->get('username');
        $result   = array();
        $response = array();
        $result   = User::deleteMultipleAccounts($user);

        if($result){
            $response['code']    = 202;
            $response['message'] = "Account has been deleted.";

            return response()->json($response,200);
        }else{
            $response['code']    = 404;
            $response['message'] = "User not found.";

            return response()->json($response,404);
        }
    }

    public function getUserList()
    {
        $result   = array();
        $response = array();
        $result   = User::getUserList();

        if(count($result) > 0){
            $response['code']    = 200;
            $response['message'] = "User list found.";
            $response['data']    = $result;

            return response()->json($response,200);
        }else{
            $response['code']    = 404;
            $response['message'] = "User not found.";
            $response['data']    = $result;

            return response()->json($response,404);
        }
    }
}
