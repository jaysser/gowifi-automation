<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\FileService as File;
use App\Services\CollectionService as Collection;

class FileController extends Controller
{
    public function __construct($username = ''){
        if(session('user_info')){
            File::setSession(session('user_info'));
            File::setUsername(current(array_keys(File::getSession())));        
        }else{
            File::setUsername($username);
        }
    }

	public static function parseConfigFile($filename,$path=0){
        if($path == 0)
    	   $result = File::parseFile($filename);
        else
           $result = File::parseFile($filename,1);

    	return $result;
    }

	public function getCollectionDirectory($path=0){
        if($path == 0)
		  $result = File::getCollectionList();
        else
          $result = File::getApiList();

		return response()->json(array('data' => $result),200);
    }

    public function createApiFile(Request $request){
        $response          = array();
        $data              = array();
        $data['api']       = $request->get('api_name');
        $data['path']      = $request->get('endpoint');
        $data['method']    = $request->get('method');
        $data['params']    = $request->get('params');

        $data     = json_encode(array($data));
        $result   = File::createFile($request->get('api_name'),json_encode(json_decode($data)),1);

        if($result == false){
            $response['code']    = 400;
            $response['message'] = "Failed to create new file";
            $response['data']    = $result; 
            return response()->json($response,400);
        }else{
            $response['code']    = 201;
            $response['message'] = "File has been created";
            $response['data']    = $result; 
            return response()->json($response,201);
        }
    }

    public function createFile(Request $request){
        $response = array();
    	$filename = $request->get('filename');
    	$data     = json_decode($request->get('data'),true);
    	$path     = $request->get('path') ? $request->get('path'):0;
        $counter  = 1;

        //clean index method
        for($i=0;$i<count($data);$i++){
            $data[$i]['method'] = $data[$i]["method_".$counter];
            unset($data[$i]['method_'.$counter]);
            $counter++;
        }

        $data     = json_encode($data);
        $error    = $this->checkDataParts(json_encode(json_decode($data)));

        if(count($error) > 0){
            if(!isset($error['url'])){
                $response['code']    = 400;
                $response['message'] = "Failed to create file.<br> Reason : missing index[".$error['missing']."] - API[".$error['api']."]";
                return response()->json($response,400);
            }else{
                $response['code']    = 400;
                $response['message'] = "Failed to create file.<br> Reason : Invalid URL - API[".$error['api'].':'.$error['key']."]";
                return response()->json($response,400);
            }
        }

    	$result   = File::createFile($filename,json_encode(json_decode($data)),$path);

    	if($result == false){
            $response['code']    = 400;
            $response['message'] = "Failed to create new file";
            $response['data']    = $result;
            return response()->json($response,400);
    	}else{
            $response['code']    = 201;
            $response['message'] = "File has been created";
            $response['data']    = $result;
            return response()->json($response,201);
    	}
    }

    public function updateApiFile(Request $request){
        $api             = $request->get('api');
        $response        = array();
        $data            = array();
        $data['api']     = $request->get('api_new') ? $request->get('api_new'):$request->get('api');
        $data['path']    = $request->get('path');
        $data['method']  = $request->get('method');
        $data['params']  = $request->get('params');

        $data            = json_encode(array($data));
        $result          = File::updateApiFile($api,json_encode(json_decode($data)));

        if($result == false){
            $response['code']    = 400;
            $response['message'] = "File failed to update.";
            $response['data']    = $result;
            return response()->json($response,400);
        }else{
            $response['code']    = 202;
            $response['message'] = "File has been updated";
            $response['data']    = $result;
            return response()->json($response,202);
        }
    }

    public function updateFile(Request $request){
        $response  = array();
    	$filename  = $request->get('filename');
    	$new_name  = $request->get('newname') ? $request->get('newname'):false;
    	$data      = $request->get('data');
        $error     = $this->checkDataParts($data);

        if(count($error) > 0){
            $response['code']    = 400;
            $response['message'] = "File failed to updated<br> Reason : missing index[".$error['missing']."] - API[".$error['api']."]";
            return response()->json($response,400);
        }

    	$result = File::updateFile($filename,$data,$new_name);

    	if($result == false){
            $response['code']    = 400;
            $response['message'] = "File failed to update.";
            $response['data']    = $result;
            return response()->json($response,400);
    	}else{
            $response['code']    = 202;
            $response['message'] = "File has been updated";
            $response['data']    = $result;
            return response()->json($response,202);
    	}
    }

    public function deleteFile(Request $request){
        $response = array();
    	$filename = $request->get('filename');
        $is_api   = $request->get('api');
    	$result   = File::deleteFile($filename,$is_api);

    	if($result == false){
            $response['code']    = 400;
            $response['message'] = "Failed to delete file";
            $response['data']    = $result;
            return response()->json($response,400);
    	}else{
            $response['code']    = 202;
            $response['message'] = "File has been deleted.";
            $response['data']    = $result;
            return response()->json($response,202);
    	}
    }

    private function checkDataParts($data){
        $result    = array();
        $params    = ['api','path','method','params'];

        $cData = json_decode($data,true);

        foreach ($params as $param) {
            foreach ($cData as $key => $value) {
                if(!array_key_exists($param, $value)){
                    $result['error']   = true;
                    $result['missing'] = $param;
                    $result['key']     = $value['api'];
                    $result['api']     = $value['api'];
                }
                elseif(array_key_exists("path", $value)){
                    if(filter_var($value["path"]['staging'], FILTER_VALIDATE_URL) === false)
                    {
                        $result['missing']  = $param;
                        $result['error']    = true;
                        $result['url']      = 'invalid';
                        $result['key']      = "staging";
                        $result['api']      = $value['api']; 
                    }

                    if(filter_var($value["path"]['production'], FILTER_VALIDATE_URL) === false)
                    {
                        $result['error']    = true;
                        $result['url']      = 'invalid';
                        $result['key']      = "production";
                        $result['api']      = $value['api'];
                    }
                }
            }
        }

        return $result;
    }
}
