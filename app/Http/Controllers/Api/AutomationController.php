<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CollectionService as Collection;
use App\Services\ApiService as Api;
use App\Services\QuickService as Quick;
use App\Services\MailService as Mailer;
use App\Services\FileService as File;
use App\Http\Controllers\Api\FileController;

class AutomationController extends Controller
{
	public function __construct(){
		if(session('user_info')){
	        File::setSession(session('user_info'));
	        File::setUsername(current(array_keys(File::getSession())));        
		}
    }

	public function runQuickTest(Request $request){
        $data            = array();
        $data['path']    = $request->get('path');
        $data['method']  = $request->get('method');
        $data['params']  = json_decode($request->get('params'),true);
        $email_address	 = $request->get('email_address') ? $request->get('email_address'):false;
        $result          = Quick::runApi(array($data));
        $response        = array();

        if(false !== $email_address){
			if(strpos($email_address, ','))
				$email_address = explode(',', $email_address);

			Mailer::sendResultsViaEmail($email_address,"Quick Test",$result);        	
        }

        if(isset($result['has_error'])){
        	$response['code']    = $result['code'];
        	$response['message'] = $result['message'];
        	$response['data']    = array("Quick Test" => $result); 
			return response()->json($response,$response['code']);
        }
		else{
			$response['code']    = 200;
			$response['message'] = "Success";
        	$response['data']    = array("quick_test" => $result); 
			return response()->json($response,200);
		}
	}

	public function runApi($api,$data_ui=array(),$email_address='',$env=0,$method=''){
		$apiData  = FileController::parseConfigFile($api,1);
		$response = array();
		
		if($method != '')
			$apiData[0]['method'] = $method;

		$result  = Api::runApi($apiData,$data_ui,$env);

		if(strpos($email_address, ','))
			$email_address = explode(',', $email_address);

		$email   = ($email_address != '' ? $email_address:env('QA_EMAIL_ADD'));

		Mailer::sendResultsViaEmail($email,$api,$result);

		if(isset($result['has_error'])){
			$response['code']    = $result['code'];
        	$response['message'] = $result['message'];
        	$response['data']    = array($api => $result); 
			return response()->json($response,$response['code']);
		}
		else{
			$response['code']    = 200;
        	$response['message'] = "Success";
        	$response['data']    = array($api => $result); 
			return response()->json($response,200);
		}
	}

	public function runApiFromUi(Request $request,$api){
		$email_address = '';
		$env           = 0;

		$email_address = $request->get('email_address') !== null ? $request->get('email_address'):null;
		$params        = json_decode($request->get('params'),true);
		$env           = $request->get('env') ? $request->get('env'):0;
		$method        = $request->get('method');

		return $this->runApi($api,$params,$email_address,$env,$method);
	}

	public function runApiCollection($collection,$data_ui=array(),$email_address,$env=0,$username=''){
		$collectionData = array();

		if(session('user_info')){
			$collectionData = FileController::parseConfigFile($collection);
		}
		else{
			$FileController = new FileController($username);
			$collectionData = $FileController::parseConfigFile($collection);
		}

		$result         = Collection::runCollection($collectionData,$data_ui,$env);
		$response		= array();

		if(strpos($email_address, ','))
			$email_address = explode(',', $email_address);

		$email          = ($email_address ? $email_address:env('QA_EMAIL_ADD'));

		Mailer::sendResultsViaEmail($email,$collection,$result);

		if(isset($result['has_error'])){
			$response['code']    = 500;
        	$response['message'] = "Internal Server Error";
        	$response['data']    = array($collection => $result); 
			return response()->json($response,500);
		}
		else{
			$response['code']    = 200;
        	$response['message'] = "Success";
        	$response['data']    = array($collection => $result); 
			return response()->json($response,200);
		}
	}

	//run automation from ui
	public function runApiCollectionFromUi(Request $request,$collection){
		$email_address = '';
		$env           = 0;

		$email_address = $request->get('email_address') !== null ? $request->get('email_address'):null;
		$params        = json_decode($request->get('params'),true);
		$env           = $request->get('env') ? $request->get('env'):0;

		return $this->runApiCollection($collection,$params,$email_address,$env);
	}
}
