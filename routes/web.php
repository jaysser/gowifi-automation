<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('view-collection');
});*/

Route::any('','Auth\LoginController@loginPage');
Route::any('logout','Auth\LoginController@logout');

Route::group(['middleware' => 'check'],function(){
  Route::get('view', function () {
      return view('view-collection');
  });

  Route::group(['prefix' => 'edit'], function () {
  	Route::get('collection/{collection}', function () {
        return view('edit-collection');
     });
  	Route::get('api/{api_name}', function () {
        return view('edit-api');
     });
  });

  Route::group(['prefix' => 'add'], function () {
  	Route::get('api', function () {
        return view('add-api');
     });
  	Route::get('collection', function () {
        return view('add-collection');
     });
  });
  Route::group(['prefix' => 'run'], function () {
  	Route::get('quick', function () {
        return view('quick-test');
     });
  	Route::get('api', function () {
        return view('api-test');
     });
  	Route::get('collection', function () {
        return view('home');
     });
  });
});
