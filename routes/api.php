<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//file management
Route::group(['prefix' => 'file'], function () {
	Route::post('create','Api\FileController@createFile');
	Route::post('create/api','Api\FileController@createApiFile');
	Route::post('update','Api\FileController@updateFile');
	Route::post('update/api','Api\FileController@updateApiFile');
	Route::post('delete','Api\FileController@deleteFile');	 	
});

//user auth
Route::group(['prefix' => 'user'], function () {
	Route::post('login','Auth\LoginController@login')->middleware('web');
	Route::post('create','Auth\RegisterController@createUser');
	Route::post('delete','Api\UserController@deleteAccount');
	Route::get('list','Api\UserController@getUserList');
});

//api/quick runner
Route::group(['prefix' => 'run'], function () {
	Route::get('api/{apiname}','Api\AutomationController@runApi');
	Route::post('api/web/{apiname}','Api\AutomationController@runApiFromUi');
	Route::post('api/quick','Api\AutomationController@runQuickTest');
});

//parser
Route::get('test/{collection}/{path?}','Api\FileController@parseConfigFile');
Route::get('get/collections/{path?}','Api\FileController@getCollectionDirectory');

//collection runner
Route::get('run-collection/{collection}','Api\AutomationController@runApiCollection');
Route::post('run-collection/web/{collection}','Api\AutomationController@runApiCollectionFromUi');

//password
Route::group(['prefix' => 'password'], function(){
	Route::post('','Auth\ResetPasswordController@changePassword');
	Route::post('forgot','Auth\ForgotPasswordController@forgotPassword');
	Route::post('reset','Auth\ResetPasswordController@resetPassword');
});